﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace BeverageApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Beverage> beverageList = new List<Beverage>();

            beverageList.Add(new Beverage { Type = "Газированный", Name = "Лимонад", Price = 15 });
            beverageList.Add(new Beverage { Type = "Газированный", Name = "Ласточка", Price = 35 });
            beverageList.Add(new Beverage { Type = "Газированный", Name = "Боржоми", Price = 95 });
            beverageList.Add(new Beverage { Type = "Газированный", Name = "Шампанское", Price = 150 });
            beverageList.Add(new Beverage { Type = "Без газа", Name = "Сок", Price = 100 });
            beverageList.Add(new Beverage { Type = "Без газа", Name = "Вода", Price = 10 });
            beverageList.Add(new Beverage { Type = "Без газа", Name = "Компот", Price = 60 });
            beverageList.Add(new Beverage { Type = "Без газа", Name = "Кисель", Price = 30 });


            var beverageListLymbda = beverageList.Where(s => s.Price > 50 && s.Type == "Без газа");

            foreach (var item in beverageListLymbda)
            {
                Console.WriteLine($"Напиток: {item.Name}. Цена: {item.Price}");
            }

            foreach (var item in beverageList)
            {
                if (item.Name == "Сок")
                {
                    item.Price += 10;
                    break;
                }
            }

            List<BeverageExpirationDate> beverageExpirationDateList = new List<BeverageExpirationDate>(beverageList.Count);

            foreach (var item in beverageList)
            {
                var beverage = new BeverageExpirationDate() { Name = item.Name, Type = item.Type, Price = item.Price, ExpiretionDate = "31-12-2021" };
                beverageExpirationDateList.Add(beverage);
            }

            Console.WriteLine("----------------------");

            foreach (var item in beverageExpirationDateList)
            {
                Console.WriteLine($"Напиток: {item.Name}. Цена: {item.Price}. Тип: {item.Type}. Срок годности: {item.ExpiretionDate}");
            }


            Console.ReadKey();
        }
    }
}
