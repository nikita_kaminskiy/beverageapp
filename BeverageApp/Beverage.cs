﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeverageApp
{
    public class Beverage
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }


    }
}
