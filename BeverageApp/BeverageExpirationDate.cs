﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeverageApp
{
    class BeverageExpirationDate
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string ExpiretionDate { get; set; }

    }
}
